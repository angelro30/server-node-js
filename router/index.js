import express from "express";

export const router = express.Router();

export default { router };

// Configurar primer ruta
router.get("/", (req, res) => {
  res.render("index", { titulo: "Practicas de nodeJS" });
});

router.get("/tabla", (req, res) => {
  const params = {
    numero: req.query.numero,
  };
  res.render("partials/practicas/tabla", params);
});

router.post("/tabla", (req, res) => {
  const params = {
    numero: req.body.numero,
  };
  res.render("partials/practicas/tabla", params);
});


//Practica cotizacion

router.get("/cotizacion", (req, res) => {
  const params = {
    valor: req.query.valor,
    pInicial: req.query.pInicial,
    plazos: req.query.plazos,
  };
  res.render("partials/practicas/cotizacion", params);
});

router.post("/cotizacion", (req, res) => {
  const params = {
    valor: req.body.valor,
    pInicial: req.body.pInicial,
    plazos: req.body.plazos,
  };
  res.render("partials/practicas/cotizacion", params);
});

// Practica alumnos
import alumnosDB from "../models/alumnos.js";

router.get("/alumnos", async (req, res) => {
  const rows = await alumnosDB.mostrarTodos();
  res.render("partials/practicas/alumnos", {regs: rows});
});

router.post("/alumnos", async (req, res) => {
  try {
    const alumnoData = {
      matricula: req.body.matricula,
      nombre: req.body.nombre,
      domicilio: req.body.domicilio,
      sexo: req.body.sexo,
      especialidad: req.body.especialidad,
    };
    const result = await alumnosDB.insertar(alumnoData);
    console.log({ result });
  } catch (error) {
    console.error(`Sucedio un error al intentar insertar alumno desde router/index.js ${error}`);
    res.status(400).send("Sucedio un error: " + error);
  }
  res.redirect('/alumnos');
});
